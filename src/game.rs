use crate::ai_entity::AiEntity;
use crate::entity::{ActionByEyeRays, EntityAction};
use crate::game_object::*;
use crate::math::number::mix;
use crate::math::vector2::Vector2;
use crate::math::vector3::Vector3;
use crate::math::vector4::Vector4;
use crate::paint::camera::Camera;
use crate::paint::painter::Painter;
use crate::user_entity::UserEntity;
use crate::world::*;
use rand::random;
extern crate scoped_pool;
use glium::Frame;
use scoped_pool::Pool;
use std::f32;

pub struct Game {
    world: World,
    game_objects: Vec<Box<dyn GameObject + Send>>,

    training_for_ai: Vec<ActionByEyeRays>,
}

impl Game {
    pub fn new() -> Game {
        let mut game = Game {
            world: World::new(),
            game_objects: Vec::new(),
            training_for_ai: Vec::new(),
        };

        let user_pos = Vector2::new(mix(game.world.rect.left(), game.world.rect.right(), random()), mix(game.world.rect.top(), game.world.rect.bottom(), random()));
        let user_direction = mix(0.0, f32::consts::PI, random());
        let user = Box::new(UserEntity::new(user_pos, user_direction, &mut game.world));

        game.world.objects_types_by_coliders.insert(user.entity.colider_handle, ObjectType::Entity);
        game.game_objects.push(user);

        for _ in 0..5 {
            let ai_pos = Vector2::new(mix(game.world.rect.left(), game.world.rect.right(), random()), mix(game.world.rect.top(), game.world.rect.bottom(), random()));
            let ai_direction = mix(0.0, f32::consts::PI, random());
            let ai = Box::new(AiEntity::new(ai_pos, ai_direction, &mut game.world));
            game.world.objects_types_by_coliders.insert(ai.entity.colider_handle, ObjectType::Entity);
            game.game_objects.push(ai);
        }

        game
    }

    pub fn step(&mut self, user_action_changing: Option<EntityAction>, elapsed_micros_from_begin: u64, dt_micros: u32, thread_pool: &mut Pool, camera: &mut Camera) {
        self.world.elapsed_micros_from_begin = elapsed_micros_from_begin;
        self.world.dt_micros = dt_micros;

        self.world.mechanical_world.set_timestep(dt_micros as f32 * 0.000_001);
        self.world.mechanical_world.step(
            &mut self.world.geometrical_world,
            &mut self.world.bodies,
            &mut self.world.colliders,
            &mut self.world.joint_constraints,
            &mut self.world.force_generators,
        );

        thread_pool.scoped(|scope| {
            let world = &self.world;
            let training_for_ai = &self.training_for_ai;

            for game_object in self.game_objects.iter_mut() {
                scope.execute(move || {
                    if let Some(new_action) = user_action_changing {
                        game_object.set_action_by_keyboard(new_action);
                    }

                    game_object.step_multithreaded(world, &training_for_ai);
                });
            }
        });

        for game_object in self.game_objects.iter_mut() {
            if let Some(action) = game_object.step(&mut self.world, camera) {
                self.training_for_ai.push(action)
            }
        }
    }

    pub fn draw(&self, painter: &mut Painter, surface: &mut Frame, show_debug_info: bool) {
        self.world.draw(painter, surface);

        for game_object in self.game_objects.iter() {
            game_object.draw(&self.world, painter, surface);
        }

        if show_debug_info {
            painter.draw_text(&format!("TRAINING STEPS COUNT: {:?}", self.training_for_ai.len()), Vector3::new(0.0, 0.9, 0.0), 0.05, Vector4::new(0.2, 1.7, 1.0, 1.0), surface);
        }
    }
}
