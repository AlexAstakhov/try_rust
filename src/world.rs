use crate::math::line2::Line2;
use crate::math::number::*;
use crate::math::rect2::Rect2;
use crate::math::vector2::*;
use crate::math::vector4::Vector4;
use crate::maze_generator::generate_maze;
use crate::paint::painter::*;
use glium::Frame;
extern crate ncollide2d;
use crate::game_object::*;
use ncollide2d::shape::{Ball, Cuboid, ShapeHandle};
extern crate nphysics2d;
use nphysics2d::force_generator::DefaultForceGeneratorSet;
use nphysics2d::joint::DefaultJointConstraintSet;
use nphysics2d::object::{BodyPartHandle, ColliderDesc, DefaultBodySet, DefaultColliderHandle, DefaultColliderSet, Ground};
use nphysics2d::world::{DefaultGeometricalWorld, DefaultMechanicalWorld};
extern crate nalgebra as na;
use rand::random;
use std::collections::HashMap;

const WALL_WIDTH: f32 = 1.0;

pub struct World {
    pub mechanical_world: DefaultMechanicalWorld<f32>,
    pub geometrical_world: DefaultGeometricalWorld<f32>,
    pub bodies: DefaultBodySet<f32>,
    pub colliders: DefaultColliderSet<f32>,
    pub joint_constraints: DefaultJointConstraintSet<f32>,
    pub force_generators: DefaultForceGeneratorSet<f32>,

    pub rect: Rect2,

    pub walls: Vec<Line2>,
    pub money: Vec<Vector2>,

    pub elapsed_micros_from_begin: u64,
    pub dt_micros: u32,

    pub objects_types_by_coliders: HashMap<DefaultColliderHandle, ObjectType>,
}

impl World {
    pub fn new() -> World {
        let mechanical_world = DefaultMechanicalWorld::new(na::Vector2::new(0.0, 0.0));
        let geometrical_world = DefaultGeometricalWorld::new();

        let mut bodies = DefaultBodySet::new();
        let mut colliders = DefaultColliderSet::new();
        let joint_constraints = DefaultJointConstraintSet::new();
        let force_generators = DefaultForceGeneratorSet::new();

        let mut walls = Vec::new();

        let maze_width = 17;
        let maze_height = 17;
        let maze = generate_maze(maze_width as usize, maze_height as usize);

        let maze_scale = 30.0;

        for x in 0..maze_width {
            for y in 0..maze_height {
                let i = to_one_dimensional(x, y, maze_width);

                let wall = !maze[i as usize];
                if wall {
                    if x < maze_width - 1 {
                        let wall_to_right = !maze[to_one_dimensional(x + 1, y, maze_width) as usize];
                        if wall_to_right {
                            let p1 = Vector2::new(x as f32 * maze_scale, y as f32 * maze_scale);
                            let p2 = Vector2::new((x + 1) as f32 * maze_scale, y as f32 * maze_scale);
                            walls.push(Line2::new(p1, p2));
                        }
                    }

                    if y < maze_height - 1 {
                        let wall_to_bottom = !maze[to_one_dimensional(x, y + 1, maze_width) as usize];
                        if wall_to_bottom {
                            let p1 = Vector2::new(x as f32 * maze_scale, y as f32 * maze_scale);
                            let p2 = Vector2::new(x as f32 * maze_scale, (y + 1) as f32 * maze_scale);
                            walls.push(Line2::new(p1, p2));
                        }
                    }
                }
            }
        }

        let mut objects_types_by_phys_bodies = HashMap::new();

        for wall in walls.iter() {
            let line_vec = wall.p2 - wall.p1;

            let line_len = line_vec.magnitude();
            let line_geom = ShapeHandle::new(Cuboid::new(na::Vector2::new(WALL_WIDTH, line_len / 2.0)));

            let angle = -line_vec.angle_to_other_as_0_2pi(Vector2::new(0.0, 1.0));
            let line_center = vector2_mix(wall.p2, wall.p1, 0.5);
            let line_pos = na::Isometry2::new(line_center, angle);

            let collider_margine = 0.001;

            let ground_handle = bodies.insert(Ground::new());

            let collider_handle = colliders.insert(ColliderDesc::new(line_geom).position(line_pos).margin(collider_margine).build(BodyPartHandle(ground_handle, 0)));

            objects_types_by_phys_bodies.insert(collider_handle, ObjectType::Wall);
        }

        let world_width = maze_width as f32 * maze_scale - maze_scale;
        let world_height = maze_height as f32 * maze_scale - maze_scale;
        let world_rect = Rect2::new(Vector2::new(world_width / 2.0, world_height / 2.0), world_width, world_height);

        let mut money = vec![];
        for _ in 0..100 {
            let pos = Vector2::new(mix(0.0, world_width, random()), mix(0.0, world_height, random()));
            money.push(pos);

            let geom = ShapeHandle::new(Ball::new(3.0));
            let iso_pos = pos.to_isometry(0.0);
            let ground_handle = bodies.insert(Ground::new());
            let collider_handle = colliders.insert(ColliderDesc::new(geom).position(iso_pos).sensor(true).build(BodyPartHandle(ground_handle, 0)));

            objects_types_by_phys_bodies.insert(collider_handle, ObjectType::Money);
        }

        World {
            mechanical_world: mechanical_world,
            geometrical_world: geometrical_world,
            bodies: bodies,
            colliders: colliders,
            joint_constraints: joint_constraints,
            force_generators: force_generators,

            rect: world_rect,

            walls: walls,
            money: money,

            elapsed_micros_from_begin: 0,
            dt_micros: 0,

            objects_types_by_coliders: objects_types_by_phys_bodies,
        }
    }

    pub fn draw(&self, painter: &mut Painter, surface: &mut Frame) {
        for wall in &self.walls {
            painter.draw_line(*wall, WALL_WIDTH, Vector4::new(0.3, 0.2, 0.8, 1.0), surface);
        }

        for money in &self.money {
            painter.draw_rect(Rect2::new(*money, 3.0, 3.0), Vector4::new(0.7, 0.7, 0.0, 1.0), surface);
        }
    }
}
