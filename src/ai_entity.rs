use crate::entity::*;
use crate::game_object::*;
use crate::math::number::scaled;
use crate::math::vector2::Vector2;
use crate::math::vector4::Vector4;
use crate::paint::camera::Camera;
use crate::paint::painter::Painter;
use crate::world::*;
use glium::Frame;
use std::f32;

pub struct AiEntity {
    pub entity: Entity,
}

impl AiEntity {
    pub fn new(pos: Vector2, direction: f32, world: &mut World) -> AiEntity {
        AiEntity {
            entity: Entity::new(pos, direction, Vector4::new(0.8, 0.2, 0.15, 1.0), world),
        }
    }
}

impl GameObject for AiEntity {
    fn step_multithreaded(&mut self, world: &World, training_for_ai: &[ActionByEyeRays]) {
        let cur_step_rays_result = self.entity.look(world);

        self.entity.action = EntityAction::Nothing;

        let mut min_diff_sum = f32::MAX;
        let mut same_what_see_cnt = 0;

        for step_from_training in training_for_ai.iter() {
            let mut diff_sum = 0.0;

            for (cur_step_ray_result, training_ray_result) in cur_step_rays_result.iter().zip(step_from_training.eye_rays_len.iter()) {
                if cur_step_ray_result.object_type == training_ray_result.object_type {
                    diff_sum += (cur_step_ray_result.len - training_ray_result.len).abs();
                    same_what_see_cnt += 1;
                }
            }

            let diff_sum_scaled_by_same = scaled(diff_sum, 0.0, same_what_see_cnt as f32, 0.0, EYE_RAYS_COUNT as f32);
            if diff_sum_scaled_by_same as f32 <= min_diff_sum {
                min_diff_sum = diff_sum_scaled_by_same;
                self.entity.action = step_from_training.action;
            }
        }
    }

    fn step(&mut self, world: &mut World, _: &mut Camera) -> Option<ActionByEyeRays> {
        self.entity.step(world);

        None
    }

    fn draw(&self, world: &World, painter: &mut Painter, surface: &mut Frame) {
        self.entity.draw(world, painter, surface);
    }

    fn i_am_alive(&self) -> bool {
        true
    }
}
