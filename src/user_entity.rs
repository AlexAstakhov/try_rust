use crate::entity::*;
use crate::game_object::*;
use crate::math::vector2::Vector2;
use crate::math::vector4::Vector4;
use crate::paint::camera::Camera;
use crate::paint::painter::Painter;
use crate::world::*;
use glium::Frame;
use std::f32;

pub struct UserEntity {
    pub entity: Entity,

    user_action_by_eye_rays: ActionByEyeRays, //calc in step_multithreaded and returned from step, need for lock-free
}

impl UserEntity {
    pub fn new(pos: Vector2, direction: f32, world: &mut World) -> UserEntity {
        UserEntity {
            entity: Entity::new(pos, direction, Vector4::new(0.2, 0.2, 1.0, 1.0), world),

            user_action_by_eye_rays: ActionByEyeRays {
                action: EntityAction::Nothing,
                eye_rays_len: [EyeRayResult {
                    object_type: ObjectType::Unknown,
                    len: f32::MAX,
                }; EYE_RAYS_COUNT],
            },
        }
    }
}

impl GameObject for UserEntity {
    fn step_multithreaded(&mut self, world: &World, _training_for_ai: &[ActionByEyeRays]) {
        self.user_action_by_eye_rays = ActionByEyeRays {
            action: self.entity.action,
            eye_rays_len: self.entity.look(world),
        };
    }

    fn step(&mut self, world: &mut World, camera: &mut Camera) -> Option<ActionByEyeRays> {
        self.entity.step(world);
        let rigid_body = world.bodies.rigid_body(self.entity.phys_body_handle).expect("Rigid-body not found.");
        camera.position.x = rigid_body.position().translation.vector.x;
        camera.position.y = rigid_body.position().translation.vector.y;
        camera.position.z = -120.0;

        Some(self.user_action_by_eye_rays)
    }

    fn draw(&self, world: &World, painter: &mut Painter, surface: &mut Frame) {
        self.entity.draw(world, painter, surface);
    }

    fn i_am_alive(&self) -> bool {
        true
    }

    fn set_action_by_keyboard(&mut self, action: EntityAction) {
        self.entity.action = action;
    }
}
