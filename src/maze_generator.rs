use rand::Rng;
use std::cell::RefCell;
use std::option::Option;
use std::rc::Rc;

/// Returns random maze as Vec<bool> where true if empty and false if wall
/// Depth-first search maze form wikipedia. Code by Jacek Wieczorek translated to rust
/// https://en.wikipedia.org/wiki/Maze_generation_algorithm
///
/// # Example
///
/// let width = 33;
/// let height = 33;
/// let maze = generate_maze(width, height);
///
/// for y in 0..height
/// {
///     let mut line_str = String::new();
///     for x in 0..width
///     {
///         let i = x + width * y; //to one dimensional
///         line_str += if maze[i] { " " } else { "#" };
///     }
///
///     println!("{:?}", line_str);
/// }
pub fn generate_maze(width: usize, height: usize) -> Vec<bool> {
    // Place on field (wall or empty) and data need for 'generate_maze'
    struct MazeNode {
        pub wall: bool,

        x: usize,
        y: usize,
        parent: Option<Rc<RefCell<MazeNode>>>,
        dirs: u8, //Directions that still haven't been explored
    }

    let size = width * height;

    // init
    let mut nodes: Vec<Rc<RefCell<MazeNode>>> = Vec::with_capacity(size);
    for i in 0..size {
        let x = i % width;
        let y = (i - x) / width;

        nodes.push(Rc::new(RefCell::new(if x * y % 2 != 0 {
            MazeNode {
                wall: false,
                x: x,
                y: y,
                parent: None,
                dirs: 15,
            }
        } else {
            MazeNode { wall: true, x: 0, y: 0, parent: None, dirs: 0 }
        })));
    }

    if width < 2 || height < 2 {
        return to_result(nodes);
    }

    let start = nodes[1 + width].clone();
    start.borrow_mut().parent = Some(start.clone());
    let mut last = start.clone();

    //Connects node to random neighbor (if possible) and returns address of next node that should be visited
    let mut link = |node: &mut Rc<RefCell<MazeNode>>| -> Option<Rc<RefCell<MazeNode>>> {
        let mut x = 0;
        let mut y = 0;

        let mut n = node.borrow_mut(); //for no use borrow() and borrow_mut() everywhere

        //While there are directions still unexplored
        while n.dirs != 0 {
            //Randomly pick one direction
            let dir = 1 << rand::thread_rng().gen_range(0, 4);

            //If it has already been explored - try again
            if !n.dirs & dir != 0 {
                continue;
            }

            //Mark direction as explored
            n.dirs &= !dir;

            //Depending on chosen direction
            match dir {
                //Check if it's possible to go right
                1 => {
                    if n.x + 2 < width {
                        x = n.x + 2;
                        y = n.y;
                    } else {
                        continue;
                    }
                }

                //Check if it's possible to go down
                2 => {
                    if n.y + 2 < height {
                        x = n.x;
                        y = n.y + 2;
                    } else {
                        continue;
                    }
                }

                //Check if it's possible to go left
                4 => {
                    if let Some(sub_res) = n.x.checked_sub(2) {
                        x = sub_res;
                        y = n.y;
                    } else {
                        continue;
                    }
                }

                //Check if it's possible to go up
                8 => {
                    if let Some(sub_res) = n.y.checked_sub(2) {
                        x = n.x;
                        y = sub_res;
                    } else {
                        continue;
                    }
                }

                _ => (),
            }

            //Get destination node into pointer (makes things a tiny bit faster)
            let dest = nodes[x + y * width].clone();

            //Make sure that destination node is not a wall
            if !dest.borrow().wall {
                //If destination is a linked node already - abort
                if dest.borrow().parent.is_some() {
                    continue;
                }

                //Otherwise, adopt node
                dest.borrow_mut().parent = Some(node.clone());

                //Remove wall between nodes
                let w_x = if x > n.x { n.x + (x - n.x) / 2 } else { x + (n.x - x) / 2 };
                let w_y = if y > n.y { n.y + (y - n.y) / 2 } else { y + (n.y - y) / 2 };
                nodes[w_x + w_y * width].borrow_mut().wall = false;

                //Return address of the child node
                return Some(dest);
            }
        }

        //If nothing more can be done here - return parent's address
        if let Some(parent) = &n.parent {
            if parent.as_ptr().ne(&start.as_ptr()) {
                //Return address of the child node
                return Some(parent.clone());
            }
        }

        //Start node is reached and can't be left
        return None;
    };

    //Connect nodes until start node is reached and can't be left
    while let Some(new_last) = link(&mut last) {
        last = new_last;
    }

    // function for convert result
    fn to_result(mut nodes: Vec<Rc<RefCell<MazeNode>>>) -> Vec<bool> {
        let mut result = Vec::with_capacity(nodes.len());

        for node in nodes.iter_mut() {
            result.push(!node.borrow().wall);

            //PREVENT MEMORY LEAK WITH REFERENCE CYCLE!
            node.borrow_mut().parent = None;
        }

        result
    };

    to_result(nodes)
}
