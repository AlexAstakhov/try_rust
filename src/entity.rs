use crate::game_object::*;
use crate::math::line2::*;
use crate::math::number::*;
use crate::math::rect2::Rect2;
use crate::math::vector2::*;
use crate::math::vector3::Vector3;
use crate::math::vector4::Vector4;
use crate::paint::painter::Painter;
use crate::world::*;
use glium::Frame;
use std::f32;
extern crate nphysics2d;
use nphysics2d::material::{BasicMaterial, MaterialHandle};
use nphysics2d::math::Velocity;
use nphysics2d::object::{BodyPartHandle, BodyStatus, ColliderDesc, DefaultBodyHandle, DefaultColliderHandle, RigidBody, RigidBodyDesc};
use nphysics2d::volumetric::Volumetric;

use ncollide2d::pipeline::object::CollisionGroups;
use ncollide2d::query::Ray;
use ncollide2d::shape::{Cuboid, ShapeHandle};
extern crate nalgebra as na;

const SPEED: f32 = 80.0;
const VISION_RANGE_ANGLE: f32 = f32::consts::PI / 2.0;
const MAX_VISION_DISTANCE: f32 = 100_000.0;

pub struct Entity {
    pub action: EntityAction,

    current_step_debug_eye_rays: [DebugEyeRay; EYE_RAYS_COUNT], // it's calculate in step method and use when draw

    pub colider_handle: DefaultColliderHandle,
    pub phys_body_handle: DefaultBodyHandle,

    body_color: Vector4,
}

impl Entity {
    pub fn new(pos: Vector2, direction: f32, body_color: Vector4, world: &mut World) -> Entity {
        let collider_margine = 0.001;

        let geom = ShapeHandle::new(Cuboid::new(na::Vector2::new(5.7, 2.8)));
        let center_of_mass = geom.center_of_mass();

        let velocity = Vector2::new(SPEED, 0.0).rotated(direction);

        let rigid_body = RigidBodyDesc::new()
            .status(BodyStatus::Dynamic)
            .gravity_enabled(true)
            .mass(1.0)
            .translation(pos)
            .rotation(direction)
            .local_center_of_mass(center_of_mass)
            .set_velocity(Velocity::linear(velocity.x, velocity.y))
            .build();

        let phys_body_handle = world.bodies.insert(rigid_body);
        let colider_handle = world.colliders.insert(
            ColliderDesc::new(geom.clone())
                .margin(collider_margine)
                .density(1.0)
                .material(MaterialHandle::new(BasicMaterial::new(0.05, 0.5)))
                .build(BodyPartHandle(phys_body_handle, 0)),
        );

        Entity {
            action: EntityAction::Nothing,

            current_step_debug_eye_rays: [DebugEyeRay {
                line: Line2::new(pos, pos + velocity.with_magnitude(MAX_VISION_DISTANCE)),
                what_see: ObjectType::Unknown,
            }; EYE_RAYS_COUNT],

            colider_handle,
            phys_body_handle,

            body_color,
        }
    }

    pub fn look(&mut self, world: &World) -> [EyeRayResult; EYE_RAYS_COUNT] {
        let mut result = [EyeRayResult {
            object_type: ObjectType::Unknown,
            len: MAX_VISION_DISTANCE,
        }; EYE_RAYS_COUNT];

        let rigid_body = world.bodies.rigid_body(self.phys_body_handle).expect("Rigid-body not found.");
        let pos = rigid_body.position().translation.vector;
        let direction = vector2_from_angle(rigid_body.position().rotation.angle());

        for (i, (ray_result, debug_eye_ray)) in result.iter_mut().zip(self.current_step_debug_eye_rays.iter_mut()).enumerate() {
            let half_vision_range_angle = VISION_RANGE_ANGLE / 2.0;
            let angle = scaled(i as f32, 0.0, (EYE_RAYS_COUNT - 1) as f32, -half_vision_range_angle, half_vision_range_angle);
            let ray_vector = direction.rotated(angle);
            let ray = Ray::new(pos.to_point(), ray_vector.normalize());

            let mut nearest_intersection = MAX_VISION_DISTANCE;
            let mut object_type_of_nearest = ObjectType::Unknown;

            for (obj_index, _, intersection) in world.geometrical_world.interferences_with_ray(&world.colliders, &ray, &CollisionGroups::new()) {
                if intersection.toi < nearest_intersection && obj_index != self.phys_body_handle {
                    nearest_intersection = intersection.toi;
                    object_type_of_nearest = *world.objects_types_by_coliders.get(&obj_index).unwrap_or(&ObjectType::Unknown);
                }
            }

            let ray_vector_of_nearest_intersection = ray_vector.with_magnitude(nearest_intersection);
            ray_result.len = nearest_intersection;
            ray_result.object_type = object_type_of_nearest;

            *debug_eye_ray = DebugEyeRay {
                line: Line2::new(pos, pos + ray_vector_of_nearest_intersection),
                what_see: object_type_of_nearest,
            };
        }

        result
    }

    pub fn step(&mut self, world: &mut World) {
        let rigid_body = world.bodies.rigid_body_mut(self.phys_body_handle).expect("Rigid-body not found.");

        match self.action {
            EntityAction::Nothing => {}
            EntityAction::Left => {
                self.turn_left(rigid_body, world.dt_micros);
            }
            EntityAction::Right => {
                self.turn_right(rigid_body, world.dt_micros);
            }
        }

        let mut linear_velocity = rigid_body.velocity().linear;
        let current_speed = linear_velocity.magnitude();
        if current_speed < SPEED {
            let new_speed = SPEED.min(current_speed + world.dt_micros as f32 * 0.00005);
            linear_velocity = linear_velocity.with_magnitude(new_speed);
            rigid_body.set_linear_velocity(linear_velocity);
        }

        // go direction to velocity
        let pos = rigid_body.position().clone();
        let direction = vector2_from_angle(pos.rotation.angle());
        let new_angle = vector2_slerp(direction, linear_velocity, world.dt_micros as f32 * 0.000_000_3).angle_from_1_0();
        if !new_angle.is_nan() {
            rigid_body.set_position(na::Isometry2::new(pos.translation.vector, new_angle));
        } else {
            rigid_body.set_position(na::Isometry2::new(pos.translation.vector, linear_velocity.angle_from_1_0()));
        }
    }

    fn turn_left(&mut self, rigid_body: &mut RigidBody<f32>, dt: u32) {
        let linear_velocity = rigid_body.velocity().linear;
        let rotated_velocity = linear_velocity.rotated(-self.angle_of_turn(linear_velocity, dt));
        rigid_body.set_linear_velocity(rotated_velocity);
    }

    fn turn_right(&mut self, rigid_body: &mut RigidBody<f32>, dt: u32) {
        let linear_velocity = rigid_body.velocity().linear;
        let rotated_velocity = linear_velocity.rotated(self.angle_of_turn(linear_velocity, dt));
        rigid_body.set_linear_velocity(rotated_velocity);
    }

    fn angle_of_turn(&self, linear_velocity: Vector2, dt: u32) -> f32 {
        linear_velocity.magnitude() * (0.000_000_06 * dt as f32)
    }

    pub fn draw(&self, world: &World, painter: &mut Painter, surface: &mut Frame) {
        let rigid_body = world.bodies.rigid_body(self.phys_body_handle).expect("Rigid-body not found.");
        let pos = rigid_body.position().translation.vector;
        let direction = vector2_from_angle(rigid_body.position().rotation.angle()).to_vector3(0.0);

        for debug_ray in self.current_step_debug_eye_rays.iter() {
            painter.draw_line(debug_ray.line, 0.5, Vector4::new(1.0, 1.0, 0.5, 0.8), surface);

            let rect = Rect2::new(debug_ray.line.p2, 1.5, 1.5);
            let intersection_color = match debug_ray.what_see {
                ObjectType::Wall => Vector4::new(1.0, 1.0, 0.0, 0.8),
                ObjectType::Entity => Vector4::new(0.7, 0.0, 0.3, 0.7),
                ObjectType::Money => Vector4::new(0.0, 1.0, 0.0, 0.7),
                _ => Vector4::new(1.0, 0.0, 0.0, 0.7),
            };
            painter.draw_rect(rect, intersection_color, surface);
        }

        painter.draw_obj(pos.to_vector3(0.0), direction, 0.0, Vector3::new(3.0, 3.0, 3.0), self.body_color, surface);
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum EntityAction {
    Nothing,
    Left,
    Right,
}

pub const EYE_RAYS_COUNT: usize = 16;

#[derive(Copy, Clone)]
pub struct EyeRayResult {
    pub object_type: ObjectType,
    pub len: f32,
}

#[derive(Copy, Clone)]
pub struct ActionByEyeRays {
    pub action: EntityAction,
    pub eye_rays_len: [EyeRayResult; EYE_RAYS_COUNT],
}

#[derive(Clone, Copy, Debug)]
struct DebugEyeRay {
    line: Line2,
    what_see: ObjectType,
}
