use crate::entity::{ActionByEyeRays, EntityAction};
use crate::paint::camera::Camera;
use crate::paint::painter::Painter;
use crate::world::*;
use glium::Frame;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ObjectType {
    Unknown,
    Wall,
    Entity,
    Money,
}

pub trait GameObject {
    // step object without interaction, need for multithread. training_for_ai need only for ai.
    fn step_multithreaded(&mut self, world: &World, training_for_ai: &[ActionByEyeRays]);

    // camera set by user. Returns Some(ActionByEyeRays) if user, None if ai.
    fn step(&mut self, world: &mut World, camera: &mut Camera) -> Option<ActionByEyeRays>;

    fn draw(&self, world: &World, painter: &mut Painter, surface: &mut Frame);

    // for remove object after step and draw methods
    fn i_am_alive(&self) -> bool;

    // only for user
    fn set_action_by_keyboard(&mut self, _: EntityAction) {}
}
