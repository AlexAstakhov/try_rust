pub struct Elapsed {
    started: bool,
    start_time: u64,
}

#[allow(dead_code)]
impl Elapsed {
    pub fn new() -> Elapsed {
        Elapsed { started: false, start_time: 0 }
    }

    pub fn start(&mut self, elapsed_millis_from_begin: u64) {
        self.started = true;
        self.start_time = elapsed_millis_from_begin;
    }

    pub fn elapsed(&mut self, elapsed_millis_from_begin: u64) -> u64 {
        if !self.started {
            self.start(elapsed_millis_from_begin);
            return 0;
        }

        elapsed_millis_from_begin - self.start_time
    }
}
