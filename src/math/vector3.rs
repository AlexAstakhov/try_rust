use crate::math::number::mix;
use std::f32;
extern crate nalgebra as na;

pub type Vector3 = na::Vector3<f32>;

pub trait MyVector3 {
    fn length_squared(&self) -> f32;
    fn with_magnitude(&self, length: f32) -> Vector3;
}

impl MyVector3 for Vector3 {
    fn length_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    fn with_magnitude(&self, length: f32) -> Vector3 {
        self.normalize() * length
    }
}

// lerp
#[allow(dead_code)]
pub fn vector3_mix(v1: Vector3, v2: Vector3, a: f32) -> Vector3 {
    Vector3::new(mix(v1.x, v2.x, a), mix(v1.y, v2.y, a), mix(v1.z, v2.z, a))
}

#[allow(dead_code)]
pub fn vector3_slerp(from: Vector3, to: Vector3, a: f32) -> Vector3 {
    if a == 0.0 {
        return from;
    }
    if from == to || a == 1.0 {
        return to;
    }

    let theta = (Vector3::dot(&from, &to) / (from.magnitude() * to.magnitude())).acos();

    if theta == 0.0 {
        return to;
    }

    let sin_theta = theta.sin();

    from * ((1.0 - a) * theta).sin() / sin_theta + to * (a * theta).sin() / sin_theta
}
