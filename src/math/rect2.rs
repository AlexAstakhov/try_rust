use crate::math::vector2::Vector2;

#[derive(Clone, Copy, Debug)]
pub struct Rect2 {
    pub top_left: Vector2,
    pub bottom_right: Vector2,
}

#[allow(dead_code)]
impl Rect2 {
    pub fn new(center: Vector2, width: f32, height: f32) -> Rect2 {
        let half_width = width / 2.0;
        let half_height = height / 2.0;

        Rect2 {
            top_left: Vector2::new(center.x - half_width, center.y - half_height),
            bottom_right: Vector2::new(center.x + half_width, center.y + half_height),
        }
    }

    pub fn top_right(&self) -> Vector2 {
        Vector2::new(self.bottom_right.x, self.top_left.y)
    }

    pub fn bottom_left(&self) -> Vector2 {
        Vector2::new(self.top_left.x, self.bottom_right.y)
    }

    pub fn width(&self) -> f32 {
        self.bottom_right.x - self.top_left.x
    }

    pub fn height(&self) -> f32 {
        self.bottom_right.y - self.top_left.y
    }

    pub fn left(&self) -> f32 {
        self.top_left.x
    }

    pub fn right(&self) -> f32 {
        self.bottom_right.x
    }

    pub fn top(&self) -> f32 {
        self.top_left.y
    }

    pub fn bottom(&self) -> f32 {
        self.bottom_right.y
    }
}
