use crate::math::number::mix;
use crate::math::vector3::Vector3;
use std::f32;
extern crate nalgebra as na;

pub type Vector2 = na::Vector2<f32>;

pub trait MyVector2 {
    fn with_magnitude(&self, length: f32) -> Vector2;
    fn rotated(&self, angle: f32) -> Vector2;

    fn angle_from_1_0(&self) -> f32; //counterclockwise angle from vec(1, 0)
    fn angle_to_other_as_0_2pi(&self, other: Vector2) -> f32; //counterclockwise angle from 0 to 2PI

    fn to_isometry(&self, angle: f32) -> na::Isometry2<f32>;
    fn to_point(&self) -> na::Point2<f32>;
    fn to_vector3(&self, z: f32) -> Vector3;
}

impl MyVector2 for Vector2 {
    fn with_magnitude(&self, length: f32) -> Vector2 {
        self.normalize() * length
    }

    fn rotated(&self, angle: f32) -> Vector2 {
        let new_x = self.x * angle.cos() - self.y * angle.sin();
        let new_y = self.x * angle.sin() + self.y * angle.cos();

        Vector2::new(new_x, new_y)
    }

    fn angle_from_1_0(&self) -> f32 //counterclockwise angle from vec(1, 0)
    {
        f32::atan2(self.y, self.x)
    }

    fn angle_to_other_as_0_2pi(&self, other: Vector2) -> f32 //counterclockwise angle from 0 to 2PI
    {
        let dot = self.x * other.x + self.y * other.y;
        let det = self.x * other.y - self.y * other.x;
        let mut result = f32::atan2(det, dot);
        if result < 0.0 {
            result += f32::consts::PI * 2.0;
        }

        result
    }

    fn to_isometry(&self, angle: f32) -> na::Isometry2<f32> {
        na::Isometry2::new(*self, angle)
    }

    fn to_point(&self) -> na::Point2<f32> {
        na::Point2::new(self.x, self.y)
    }

    fn to_vector3(&self, z: f32) -> Vector3 {
        Vector3::new(self.x, self.y, z)
    }
}

pub fn vector2_from_angle(angle: f32) -> Vector2 {
    Vector2::new(1.0, 0.0).rotated(angle)
}

// lerp
pub fn vector2_mix(v1: Vector2, v2: Vector2, a: f32) -> Vector2 {
    Vector2::new(mix(v1.x, v2.x, a), mix(v1.y, v2.y, a))
}

pub fn vector2_slerp(from: Vector2, to: Vector2, a: f32) -> Vector2 {
    if a == 0.0 {
        return from;
    }
    if from == to || a == 1.0 {
        return to;
    }

    let theta = (Vector2::dot(&from, &to) / (from.magnitude() * to.magnitude())).acos();

    if theta == 0.0 {
        return to;
    }

    let sin_theta = theta.sin();

    from * (((1.0 - a) * theta).sin() / sin_theta) + to * (a * theta).sin() / sin_theta
}
