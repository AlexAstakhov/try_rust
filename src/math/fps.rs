const NUMBER_OF_FRAMES_FOR_AVERAGE: usize = 30;

// Average fps
pub struct Fps {
    fps: f32,
    time_of_frames_msec: [u32; NUMBER_OF_FRAMES_FOR_AVERAGE],
    time_of_last_frame_msec: u32,
    number_of_processed_frames_at_all: u64,
}

impl Fps {
    pub fn new() -> Fps {
        Fps {
            fps: 0.0,
            time_of_frames_msec: [0; NUMBER_OF_FRAMES_FOR_AVERAGE],
            time_of_last_frame_msec: 0,
            number_of_processed_frames_at_all: 0,
        }
    }

    pub fn calculate(&mut self, elapsed: u32) {
        let index_of_current_frame = self.number_of_processed_frames_at_all % NUMBER_OF_FRAMES_FOR_AVERAGE as u64;
        self.time_of_frames_msec[index_of_current_frame as usize] = if elapsed > self.time_of_last_frame_msec { elapsed - self.time_of_last_frame_msec } else { 0 };

        self.time_of_last_frame_msec = elapsed;

        self.number_of_processed_frames_at_all += 1;

        let count = if self.number_of_processed_frames_at_all < NUMBER_OF_FRAMES_FOR_AVERAGE as u64 {
            self.number_of_processed_frames_at_all
        } else {
            NUMBER_OF_FRAMES_FOR_AVERAGE as u64
        };

        self.fps = 0.0;

        for times_of_frame in self.time_of_frames_msec.iter() {
            self.fps += *times_of_frame as f32;
        }

        self.fps = 1000.0 / (self.fps / count as f32);
    }

    pub fn val(&self) -> f32 {
        self.fps
    }
}
