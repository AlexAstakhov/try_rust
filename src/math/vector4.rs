extern crate nalgebra as na;

pub type Vector4 = na::Vector4<f32>;

pub trait MyVector4 {
    fn as_array(&self) -> [f32; 4];
}

impl MyVector4 for Vector4 {
    fn as_array(&self) -> [f32; 4] {
        [self.x, self.y, self.z, self.w]
    }
}
