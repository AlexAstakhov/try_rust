pub fn mix(start_range: f32, end_range: f32, a: f32) -> f32 {
    (end_range - start_range) * a + start_range
}

pub fn normalized(number: f32, start_range: f32, end_range: f32) -> f32 {
    (number - start_range) / (end_range - start_range)
}

pub fn scaled(number: f32, start_src_range: f32, end_src_range: f32, start_dst_range: f32, end_dst_range: f32) -> f32 {
    mix(start_dst_range, end_dst_range, normalized(number, start_src_range, end_src_range))
}

pub fn fuzzy_is_null(val: f32) -> bool {
    val.abs() <= 0.00001
}

pub fn to_2_dimensional(pos: i32, field_width: i32) -> (/*x*/ i32, /*y*/ i32) {
    let x = pos % field_width;
    let y = (pos - x) / field_width;

    (x, y)
}

pub fn to_one_dimensional(x: i32, y: i32, field_width: i32) -> i32 {
    x + field_width * y
}
