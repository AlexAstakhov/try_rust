use crate::math::vector2::Vector2;

#[derive(Clone, Copy, Debug)]
pub struct Line2 {
    pub p1: Vector2,
    pub p2: Vector2,
}

#[allow(dead_code)]
impl Line2 {
    pub fn new(p1: Vector2, p2: Vector2) -> Line2 {
        Line2 { p1, p2 }
    }

    pub fn length(&self) -> f32 {
        (self.p2 - self.p1).magnitude()
    }

    pub fn intersection(&self, other: &Line2) -> (IntersectType, Vector2) {
        // implementation is based on Graphics Gems III's "Faster Line Segment Intersection"
        let a = self.p2 - self.p1;
        let b = other.p1 - other.p2;
        let c = self.p1 - other.p1;

        let denominator = a.y * b.x - a.x * b.y;
        if denominator == 0.0 || !denominator.is_finite() {
            return (IntersectType::NoIntersection, Vector2::new(0.0, 0.0));
        }

        let reciprocal = 1.0 / denominator;
        let na = (b.y * c.x - b.x * c.y) * reciprocal;
        let intersection_point = self.p1 + a * na;

        if na < 0.0 || na > 1.0 {
            return (IntersectType::Unbounded, intersection_point);
        }

        let nb = (a.x * c.y - a.y * c.x) * reciprocal;

        if nb < 0.0 || nb > 1.0 {
            return (IntersectType::Unbounded, intersection_point);
        }

        (IntersectType::Bounded, intersection_point)
    }
}

#[derive(PartialEq)]
pub enum IntersectType {
    NoIntersection,
    Bounded,
    Unbounded,
}
