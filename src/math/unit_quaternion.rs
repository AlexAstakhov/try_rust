use crate::math::number::fuzzy_is_null;
use crate::math::vector3::*;
extern crate nalgebra as na;

pub type UnitQuaternion = na::UnitQuaternion<f32>;

pub fn quaternion_from_direction(direction: Vector3, up: Vector3) -> UnitQuaternion {
    if fuzzy_is_null(direction.x) && fuzzy_is_null(direction.y) && fuzzy_is_null(direction.z) {
        return UnitQuaternion::from_quaternion(na::Quaternion::new(1.0, 0.0, 0.0, 0.0));
    }

    let z_axis = direction.normalize();

    let x_axis = Vector3::cross(&up, &z_axis);
    if fuzzy_is_null(x_axis.length_squared()) {
        return quaternion_from_rotation_to(Vector3::new(0.0, 0.0, 1.0), z_axis);
    }

    let x_axis = x_axis.normalize();
    let y_axis = Vector3::cross(&z_axis, &x_axis);

    UnitQuaternion::from_rotation_matrix(&na::Rotation3::from_matrix_unchecked(na::Matrix3::new(x_axis.x, y_axis.x, z_axis.x, x_axis.y, y_axis.y, z_axis.y, x_axis.z, y_axis.z, z_axis.z)))
}

pub fn quaternion_from_rotation_to(from: Vector3, to: Vector3) -> UnitQuaternion {
    // Based on Stan Melax's article in Game Programming Gems

    let v0 = from.normalize();
    let v1 = to.normalize();

    let d = Vector3::dot(&v0, &v1) + 1.0;

    if fuzzy_is_null(d) {
        let mut axis = Vector3::cross(&Vector3::new(1.0, 0.0, 0.0), &v0);
        if fuzzy_is_null(axis.length_squared()) {
            axis = Vector3::cross(&Vector3::new(0.0, 1.0, 0.0), &v0);
        }
        axis = axis.normalize();

        return UnitQuaternion::from_quaternion(na::Quaternion::new(0.0, axis.x, axis.y, axis.z));
    }

    let d = (2.0 * d).sqrt();
    let mut axis = Vector3::cross(&v0, &v1);
    axis.x /= d;
    axis.y /= d;
    axis.z /= d;

    return UnitQuaternion::from_quaternion(na::Quaternion::new(d * 0.5, axis.x, axis.y, axis.z).normalize());
}
