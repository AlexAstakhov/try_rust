use crate::math::unit_quaternion::*;
use crate::math::vector3::Vector3;
use std::ops::MulAssign;

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Matrix4 {
    pub m: [[f32; 4]; 4],
}

impl Matrix4 {
    pub fn new_identity() -> Matrix4 {
        Matrix4 {
            m: [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]],
        }
    }

    pub fn from_pos_rotation_scale(pos: Vector3, direction: Vector3, roll: f32, scale: Vector3) -> Matrix4 {
        let mut result = Matrix4 {
            m: [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [pos.x, pos.y, pos.z, 1.0]],
        };

        result.rotate(quaternion_from_direction(direction.normalize(), Vector3::new(0.0, 0.0, 1.0)));
        result.rotate_around(roll, Vector3::new(0.0, 0.0, 1.0));

        result.m[0][0] *= scale.x;
        result.m[0][1] *= scale.x;
        result.m[0][2] *= scale.x;
        result.m[0][3] *= scale.x;
        result.m[1][0] *= scale.y;
        result.m[1][1] *= scale.y;
        result.m[1][2] *= scale.y;
        result.m[1][3] *= scale.y;
        result.m[2][0] *= scale.z;
        result.m[2][1] *= scale.z;
        result.m[2][2] *= scale.z;
        result.m[2][3] *= scale.z;

        result
    }

    pub fn rotate_around(&mut self, angle: f32, vector: Vector3) {
        if angle == 0.0 {
            return;
        }

        let (c, mut s) = if angle == 90.0 || angle == -270.0 {
            (0.0, 1.0)
        } else if angle == -90.0 || angle == 270.0 {
            (0.0, -1.0)
        } else if angle == 180.0 || angle == -180.0 {
            (-1.0, 0.0)
        } else {
            let a = angle.to_radians();
            (a.cos(), a.sin())
        };

        let m = &mut self.m;

        if vector.x == 0.0 {
            if vector.y == 0.0 {
                if vector.z != 0.0 {
                    if vector.z < 0.0 {
                        s = -s;
                    }

                    let mut tmp = m[0][0];
                    m[0][0] = tmp * c + m[1][0] * s;
                    m[1][0] = m[1][0] * c - tmp * s;
                    tmp = m[0][1];
                    m[0][1] = tmp * c + m[1][1] * s;
                    m[1][1] = m[1][1] * c - tmp * s;
                    tmp = m[0][2];
                    m[0][2] = tmp * c + m[1][2] * s;
                    m[1][2] = m[1][2] * c - tmp * s;
                    tmp = m[0][3];
                    m[0][3] = tmp * c + m[1][3] * s;
                    m[1][3] = m[1][3] * c - tmp * s;

                    return;
                }
            } else if vector.z == 0.0 {
                if vector.y < 0.0 {
                    s = -s;
                }

                let mut tmp = m[2][0];
                m[2][0] = tmp * c + m[0][0] * s;
                m[0][0] = m[0][0] * c - tmp * s;
                tmp = m[2][1];
                m[2][1] = tmp * c + m[0][1] * s;
                m[0][1] = m[0][1] * c - tmp * s;
                tmp = m[2][2];
                m[2][2] = tmp * c + m[0][2] * s;
                m[0][2] = m[0][2] * c - tmp * s;
                tmp = m[2][3];
                m[2][3] = tmp * c + m[0][3] * s;
                m[0][3] = m[0][3] * c - tmp * s;

                return;
            }
        } else if vector.y == 0.0 && vector.z == 0.0 {
            if vector.x < 0.0 {
                s = -s;
            }

            let mut tmp = m[1][0];
            m[1][0] = tmp * c + m[2][0] * s;
            m[2][0] = m[2][0] * c - tmp * s;
            tmp = m[1][1];
            m[1][1] = tmp * c + m[2][1] * s;
            m[2][1] = m[2][1] * c - tmp * s;
            tmp = m[1][2];
            m[1][2] = tmp * c + m[2][2] * s;
            m[2][2] = m[2][2] * c - tmp * s;
            tmp = m[1][3];
            m[1][3] = tmp * c + m[2][3] * s;
            m[2][3] = m[2][3] * c - tmp * s;

            return;
        }

        let vector = vector.normalize();

        let ic = 1.0 - c;

        let m = &mut self.m;
        m[0][0] = vector.x * vector.x * ic + c;
        m[1][0] = vector.x * vector.y * ic - vector.z * s;
        m[2][0] = vector.x * vector.z * ic + vector.y * s;
        m[3][0] = 0.0;
        m[0][1] = vector.y * vector.x * ic + vector.z * s;
        m[1][1] = vector.y * vector.y * ic + c;
        m[2][1] = vector.y * vector.z * ic - vector.x * s;
        m[3][1] = 0.0;
        m[0][2] = vector.x * vector.z * ic - vector.y * s;
        m[1][2] = vector.y * vector.z * ic + vector.x * s;
        m[2][2] = vector.z * vector.z * ic + c;
        m[3][2] = 0.0;
        m[0][3] = 0.0;
        m[1][3] = 0.0;
        m[2][3] = 0.0;
        m[3][3] = 1.0;
    }

    pub fn rotate(&mut self, quaternion: UnitQuaternion) {
        // Algorithm from:
        // http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q54

        let quaternion = quaternion.as_vector();

        let f2x = quaternion.x + quaternion.x;
        let f2y = quaternion.y + quaternion.y;
        let f2z = quaternion.z + quaternion.z;
        let f2xw = f2x * quaternion.w;
        let f2yw = f2y * quaternion.w;
        let f2zw = f2z * quaternion.w;
        let f2xx = f2x * quaternion.x;
        let f2xy = f2x * quaternion.y;
        let f2xz = f2x * quaternion.z;
        let f2yy = f2y * quaternion.y;
        let f2yz = f2y * quaternion.z;
        let f2zz = f2z * quaternion.z;

        *self *= Matrix4 {
            m: [
                [1.0 - (f2yy + f2zz), f2xy + f2zw, f2xz - f2yw, 0.0],
                [f2xy - f2zw, 1.0 - (f2xx + f2zz), f2yz + f2xw, 0.0],
                [f2xz + f2yw, f2yz - f2xw, 1.0 - (f2xx + f2yy), 0.0],
                [0.0, 0.0, 0.0, 1.0],
            ],
        }
    }
}

impl MulAssign for Matrix4 {
    fn mul_assign(&mut self, other: Matrix4) {
        let m = &mut self.m;

        let m0 = m[0][0] * other.m[0][0] + m[1][0] * other.m[0][1] + m[2][0] * other.m[0][2] + m[3][0] * other.m[0][3];
        let m1 = m[0][0] * other.m[1][0] + m[1][0] * other.m[1][1] + m[2][0] * other.m[1][2] + m[3][0] * other.m[1][3];
        let m2 = m[0][0] * other.m[2][0] + m[1][0] * other.m[2][1] + m[2][0] * other.m[2][2] + m[3][0] * other.m[2][3];
        m[3][0] = m[0][0] * other.m[3][0] + m[1][0] * other.m[3][1] + m[2][0] * other.m[3][2] + m[3][0] * other.m[3][3];
        m[0][0] = m0;
        m[1][0] = m1;
        m[2][0] = m2;

        let m0 = m[0][1] * other.m[0][0] + m[1][1] * other.m[0][1] + m[2][1] * other.m[0][2] + m[3][1] * other.m[0][3];
        let m1 = m[0][1] * other.m[1][0] + m[1][1] * other.m[1][1] + m[2][1] * other.m[1][2] + m[3][1] * other.m[1][3];
        let m2 = m[0][1] * other.m[2][0] + m[1][1] * other.m[2][1] + m[2][1] * other.m[2][2] + m[3][1] * other.m[2][3];
        m[3][1] = m[0][1] * other.m[3][0] + m[1][1] * other.m[3][1] + m[2][1] * other.m[3][2] + m[3][1] * other.m[3][3];
        m[0][1] = m0;
        m[1][1] = m1;
        m[2][1] = m2;

        let m0 = m[0][2] * other.m[0][0] + m[1][2] * other.m[0][1] + m[2][2] * other.m[0][2] + m[3][2] * other.m[0][3];
        let m1 = m[0][2] * other.m[1][0] + m[1][2] * other.m[1][1] + m[2][2] * other.m[1][2] + m[3][2] * other.m[1][3];
        let m2 = m[0][2] * other.m[2][0] + m[1][2] * other.m[2][1] + m[2][2] * other.m[2][2] + m[3][2] * other.m[2][3];
        m[3][2] = m[0][2] * other.m[3][0] + m[1][2] * other.m[3][1] + m[2][2] * other.m[3][2] + m[3][2] * other.m[3][3];
        m[0][2] = m0;
        m[1][2] = m1;
        m[2][2] = m2;

        let m0 = m[0][3] * other.m[0][0] + m[1][3] * other.m[0][1] + m[2][3] * other.m[0][2] + m[3][3] * other.m[0][3];
        let m1 = m[0][3] * other.m[1][0] + m[1][3] * other.m[1][1] + m[2][3] * other.m[1][2] + m[3][3] * other.m[1][3];
        let m2 = m[0][3] * other.m[2][0] + m[1][3] * other.m[2][1] + m[2][3] * other.m[2][2] + m[3][3] * other.m[2][3];
        m[3][3] = m[0][3] * other.m[3][0] + m[1][3] * other.m[3][1] + m[2][3] * other.m[3][2] + m[3][3] * other.m[3][3];
        m[0][3] = m0;
        m[1][3] = m1;
        m[2][3] = m2;
    }
}
