use glium::{Display, Program};

pub fn one_color_program(display: &Display) -> Program {
    let vertex_shader_src = r#"
        #version 140

        in vec3 position;

        uniform mat4 perspective;
        uniform mat4 view;
        uniform mat4 model;

        void main()
        {
            mat4 modelview = view * model;
            gl_Position = perspective * modelview * vec4(position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 140

        out vec4 color;

        uniform vec4 u_color;

        void main()
        {
            color = u_color;
        }
    "#;

    glium::Program::from_source(display, vertex_shader_src, fragment_shader_src, None).unwrap()
}

pub fn material_colors_program(display: &Display) -> Program {
    let vertex_shader_src = r#"
        #version 140

        uniform mat4 u_perspective;
        uniform mat4 u_view;
        uniform mat4 u_model;

        in vec3 position;
        in vec3 normal;
        in vec3 ambient;
        in vec3 diffuse;
        in vec3 specular;

        out vec3 v_position;
        out vec3 v_normal;
        out vec3 v_ambient;
        out vec3 v_diffuse;
        out vec3 v_specular;

        void main()
        {
            v_position = position;
            v_normal = normal;
            v_ambient = ambient;
            v_diffuse = diffuse;
            v_specular = specular;
            mat4 modelview = u_view * u_model;
            gl_Position = u_perspective * modelview * vec4(v_position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 140

        in vec3 v_normal;
        in vec3 v_position;
        in vec3 v_ambient;
        in vec3 v_diffuse;
        in vec3 v_specular;

        out vec4 color;

        uniform vec3 u_light;
        uniform vec4 u_adjust_color;

        void main()
        {
            float diffuse = max(dot(normalize(v_normal), normalize(u_light)), 0.0);
            vec3 camera_dir = normalize(-v_position);
            vec3 half_direction = normalize(normalize(u_light) + camera_dir);
            float specular = pow(max(dot(half_direction, normalize(v_normal)), 0.0), 16.0);
            color = vec4(v_ambient + diffuse * v_diffuse + specular * v_specular, 1.0) * u_adjust_color;
        }
    "#;

    glium::Program::from_source(display, vertex_shader_src, fragment_shader_src, None).unwrap()
}

pub fn program_for_text(display: &Display) -> Program {
    let vertex_shader_src = r#"
        #version 140

        in vec3 position;
        in vec2 texcoord;
        
        out vec2 v_texcoord;

        void main()
        {
            v_texcoord = texcoord;
            gl_Position = vec4(position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 140

        in vec2 v_texcoord;

        out vec4 color;

        uniform sampler2D u_texture;
        uniform vec4 u_adjust_color;

        void main()
        {
            color = texture(u_texture, v_texcoord).rgba * u_adjust_color;
        }
    "#;

    glium::Program::from_source(display, vertex_shader_src, fragment_shader_src, None).unwrap()
}
