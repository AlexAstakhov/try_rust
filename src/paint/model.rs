use crate::math::matrix4::Matrix4;
use crate::math::vector3::Vector3;
use crate::math::vector4::Vector4;
use crate::paint::camera::Camera;
use std::path::Path;
extern crate tobj;
use glium::{Display, Frame, IndexBuffer, Program, Surface, VertexBuffer};

#[derive(Copy, Clone)]
pub struct VertexPNADS {
    position: [f32; 3],
    normal: [f32; 3],
    ambient: [f32; 3],
    diffuse: [f32; 3],
    specular: [f32; 3],
}
implement_vertex!(VertexPNADS, position, normal, ambient, diffuse, specular);

#[derive(Copy, Clone)]
pub struct VertexPTNADS {
    position: [f32; 3],
    texcoord: [f32; 2],
    normal: [f32; 3],
    ambient: [f32; 3],
    diffuse: [f32; 3],
    specular: [f32; 3],
}
implement_vertex!(VertexPTNADS, position, texcoord, normal, ambient, diffuse, specular);

pub struct Model {
    pub vertex: VertexBuffer<VertexPNADS>,
    pub indices: IndexBuffer<u16>,
}

impl Model {
    pub fn new(display: &Display) -> Model {
        let mut vertex_data = Vec::new();
        let mut indices_data = Vec::new();

        match tobj::load_obj(Path::new("./car_model/car.obj")) {
            Ok((models, mats)) => {
                for model in models.iter() {
                    let mesh = &model.mesh;

                    for idx in mesh.indices.iter() {
                        indices_data.push(*idx as u16);
                        let i = *idx as usize;
                        let position = if mesh.positions.len() > i * 3 + 2 {
                            [mesh.positions[3 * i], mesh.positions[3 * i + 1], mesh.positions[3 * i + 2]]
                        } else {
                            [0.0, 0.0, 0.0]
                        };

                        let normal = if mesh.normals.len() > i * 3 + 2 {
                            [mesh.normals[3 * i], mesh.normals[3 * i + 1], mesh.normals[3 * i + 2]]
                        } else {
                            [0.0, 0.0, 0.0]
                        };

                        //                        let texcoord = if mesh.texcoords.len() > i * 2 + 1
                        //                        {
                        //                            [
                        //                                 mesh.texcoords[2 * i],
                        //                                 mesh.texcoords[2 * i + 1]
                        //                            ]
                        //                        }
                        //                        else
                        //                        {
                        //                            [0.0, 0.0]
                        //                        };

                        let (ambient, diffuse, specular) = match mesh.material_id {
                            Some(i) => (mats[i].ambient, mats[i].diffuse, mats[i].specular),
                            None => ([0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
                        };

                        vertex_data.push(VertexPNADS { position, normal, ambient, diffuse, specular });
                    }
                }
            }
            Err(e) => println!("{}", e),
        }

        Model {
            vertex: glium::VertexBuffer::new(display, &vertex_data).unwrap(),
            indices: glium::IndexBuffer::new(display, glium::index::PrimitiveType::TriangleStrip, &indices_data).unwrap(),
        }
    }

    pub fn draw(&self, pos: Vector3, direction: Vector3, roll: f32, scale: Vector3, adjust_color: Vector4, camera: &Camera, program: &Program, surface: &mut Frame) {
        let model_matrix = Matrix4::from_pos_rotation_scale(pos, direction, roll, scale).m;

        let u_adj_color = [adjust_color.x, adjust_color.y, adjust_color.z, adjust_color.w];
        let uniforms = uniform! { u_perspective: camera.perspective_matrix(),
        u_view: camera.view_matrix(),
        u_model: model_matrix,
        u_light: [0.3, 0.2, 0.7f32],
        u_adjust_color:
        u_adj_color };

        surface.draw(&self.vertex, &self.indices, &program, &uniforms, &Default::default()).unwrap_or(());
    }
}
