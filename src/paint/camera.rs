use crate::math::vector3::Vector3;

pub struct Camera {
    pub position: Vector3,
    pub direction: Vector3,

    pub aspect_ratio: f32,
}

impl Camera {
    pub fn new() -> Camera {
        Camera {
            position: Vector3::new(0.0, 0.0, -100.0),
            direction: Vector3::new(0.0, 0.0, 1.0),

            aspect_ratio: 1024.0 / 768.0,
        }
    }
}

impl Camera {
    pub fn perspective_matrix(&self) -> [[f32; 4]; 4] {
        let fov: f32 = std::f32::consts::PI / 2.0;
        let far = 1024.0;
        let near = 0.1;

        let f = 1.0 / (fov / 2.0).tan();

        [
            [f / self.aspect_ratio, 0.0, 0.0, 0.0],
            [0.0, f, 0.0, 0.0],
            [0.0, 0.0, (far + near) / (far - near), 1.0],
            [0.0, 0.0, -(2.0 * far * near) / (far - near), 0.0],
        ]
    }

    pub fn view_matrix(&self) -> [[f32; 4]; 4] {
        let f = {
            let f = self.direction;
            let len = f.x * f.x + f.y * f.y + f.z * f.z;
            let len = len.sqrt();
            Vector3::new(f.x / len, f.y / len, f.z / len)
        };

        let up = Vector3::new(0.0, 1.0, 0.0);

        let s = Vector3::new(f.y * up.z - f.z * up.y, f.z * up.x - f.x * up.z, f.x * up.y - f.y * up.x);

        let s_norm = {
            let len = s.x * s.x + s.y * s.y + s.z * s.z;
            let len = len.sqrt();
            Vector3::new(s.x / len, s.y / len, s.z / len)
        };

        let u = Vector3::new(s_norm.y * f.z - s_norm.z * f.y, s_norm.z * f.x - s_norm.x * f.z, s_norm.x * f.y - s_norm.y * f.x);

        let p = Vector3::new(
            -self.position.x * s.x - self.position.y * s.y - self.position.z * s.z,
            -self.position.x * u.x - self.position.y * u.y - self.position.z * u.z,
            -self.position.x * f.x - self.position.y * f.y - self.position.z * f.z,
        );

        [[s_norm.x, u.x, f.x, 0.0], [s_norm.y, u.y, f.y, 0.0], [s_norm.z, u.z, f.z, 0.0], [p.x, p.y, p.z, 1.0]]
    }
}
