use crate::math::line2::Line2;
use crate::math::matrix4::Matrix4;
use crate::math::number::*;
use crate::math::rect2::Rect2;
use crate::math::vector2::*;
use crate::math::vector3::Vector3;
use crate::math::vector4::*;
use crate::paint::camera::Camera;
use crate::paint::model::Model;
use crate::paint::shaders::*;
use glium::{Display, Frame, IndexBuffer, Program, Surface, Texture2d, VertexBuffer};
use std::f32;
use std::io::Cursor;

#[derive(Copy, Clone)]
struct SimplestVertex {
    position: [f32; 3],
}
implement_vertex!(SimplestVertex, position);

#[derive(Copy, Clone)]
struct VertexWithTexcoord {
    position: [f32; 3],
    texcoord: [f32; 2],
}
implement_vertex!(VertexWithTexcoord, position, texcoord);

struct FontAtlas {
    texture: Texture2d,
    top_left_ascii_char: i32,
    row_cnt: i32,
    column_cnt: i32,
}

#[allow(dead_code)]
pub struct Painter {
    pub camera: Camera,

    textured_square_vertex: VertexBuffer<VertexWithTexcoord>,
    square_vertex: VertexBuffer<SimplestVertex>,

    square_indices: IndexBuffer<u16>,

    one_color_program: Program,
    program_for_obj: Program,
    program_for_text: Program,

    model: Model,

    font_atlas: FontAtlas,
}

impl Painter {
    pub fn new(display: &Display) -> Painter {
        let image = image::load(Cursor::new(&include_bytes!("font.png")[..]), image::PNG).unwrap().to_rgba();
        let image_dimensions = image.dimensions();
        let prepared_image = glium::texture::RawImage2d::from_raw_rgba_reversed(&image.into_raw(), image_dimensions);
        let font_texture = glium::texture::Texture2d::new(display, prepared_image).unwrap();

        let square_vertex = {
            let vertex1 = SimplestVertex { position: [0.5, 0.5, 0.0] };
            let vertex2 = SimplestVertex { position: [0.5, -0.5, 0.0] };
            let vertex3 = SimplestVertex { position: [-0.5, -0.5, 0.0] };
            let vertex4 = SimplestVertex { position: [-0.5, 0.5, 0.0] };
            vec![vertex1, vertex2, vertex3, vertex4]
        };

        let textured_square_vertex = {
            let vertex1 = VertexWithTexcoord {
                position: [0.5, 0.5, 0.0],
                texcoord: [1.0, 1.0],
            };
            let vertex2 = VertexWithTexcoord {
                position: [0.5, -0.5, 0.0],
                texcoord: [1.0, 0.0],
            };
            let vertex3 = VertexWithTexcoord {
                position: [-0.5, -0.5, 0.0],
                texcoord: [0.0, 0.0],
            };
            let vertex4 = VertexWithTexcoord {
                position: [-0.5, 0.5, 0.0],
                texcoord: [0.0, 1.0],
            };
            vec![vertex1, vertex2, vertex3, vertex4]
        };

        let square_indices = [0, 1, 2, 0, 2, 3];

        Painter {
            camera: Camera::new(),

            square_vertex: glium::VertexBuffer::dynamic(display, &square_vertex).unwrap(),
            textured_square_vertex: glium::VertexBuffer::dynamic(display, &textured_square_vertex).unwrap(),
            square_indices: glium::IndexBuffer::new(display, glium::index::PrimitiveType::TriangleStrip, &square_indices).unwrap(),

            one_color_program: one_color_program(display),

            program_for_text: program_for_text(display),

            program_for_obj: material_colors_program(display),
            model: Model::new(display),

            font_atlas: FontAtlas {
                texture: font_texture,
                top_left_ascii_char: 32,
                row_cnt: 8,
                column_cnt: 8,
            },
        }
    }

    pub fn draw_line(&self, line: Line2, width: f32, color: Vector4, surface: &mut Frame) {
        let uniforms = uniform! { perspective: self.camera.perspective_matrix(), view: self.camera.view_matrix(), model: Matrix4::new_identity().m, u_color: [color.x, color.y, color.z, color.w] };

        let mut tmp = (line.p2 - line.p1).rotated(f32::consts::PI / 2.0).with_magnitude(width / 2.0);
        let p1 = line.p1 + tmp;
        let p2 = line.p2 + tmp;
        tmp.x = -tmp.x;
        tmp.y = -tmp.y;
        let p3 = line.p2 + tmp;
        let p4 = line.p1 + tmp;

        let vertex1 = SimplestVertex { position: [p1.x, p1.y, 0.0] };
        let vertex2 = SimplestVertex { position: [p2.x, p2.y, 0.0] };
        let vertex3 = SimplestVertex { position: [p3.x, p3.y, 0.0] };
        let vertex4 = SimplestVertex { position: [p4.x, p4.y, 0.0] };
        let shape = vec![vertex1, vertex2, vertex3, vertex4];
        self.square_vertex.write(&shape);

        let prams = glium::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            ..Default::default()
        };
        surface.draw(&self.square_vertex, &self.square_indices, &self.one_color_program, &uniforms, &prams).unwrap_or(());
    }

    pub fn draw_rect(&self, rect: Rect2, color: Vector4, surface: &mut Frame) {
        let uniforms = uniform! { perspective: self.camera.perspective_matrix(), view: self.camera.view_matrix(), model: Matrix4::new_identity().m,
        u_color: [color.x, color.y, color.z, color.w] };

        let Rect2 { top_left, bottom_right } = rect;
        let bottom_left = rect.bottom_left();
        let top_right = rect.top_right();

        let vertex1 = SimplestVertex { position: [top_left.x, top_left.y, 0.0] };
        let vertex2 = SimplestVertex { position: [top_right.x, top_right.y, 0.0] };
        let vertex3 = SimplestVertex {
            position: [bottom_right.x, bottom_right.y, 0.0],
        };
        let vertex4 = SimplestVertex { position: [bottom_left.x, bottom_left.y, 0.0] };
        let shape = vec![vertex1, vertex2, vertex3, vertex4];
        self.square_vertex.write(&shape);

        let prams = glium::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            ..Default::default()
        };
        surface.draw(&self.square_vertex, &self.square_indices, &self.one_color_program, &uniforms, &prams).unwrap_or(());
    }

    pub fn draw_obj(&self, pos: Vector3, direction: Vector3, roll: f32, scale: Vector3, adjust_color: Vector4, frame: &mut Frame) {
        self.model.draw(pos, direction, roll, scale, adjust_color, &self.camera, &self.program_for_obj, frame);
    }

    pub fn draw_text(&self, text: &str, pos: Vector3, font_size: f32, color_adjust: Vector4, surface: &mut Frame) {
        let start_char_x = pos.x - text.len() as f32 * font_size / 2.0 + font_size / 2.0;
        for (i, ch) in text.chars().enumerate() {
            let char_pos = Vector3::new(start_char_x + font_size * i as f32, pos.y, pos.z);
            let mut ch = ch as i32;
            if ch < self.font_atlas.top_left_ascii_char || ch > self.font_atlas.top_left_ascii_char + self.font_atlas.row_cnt * self.font_atlas.column_cnt {
                println!("can't draw symbol, {:?} no in atlas", ch);
                ch = self.font_atlas.top_left_ascii_char;
            }

            self.draw_symbol(ch as u8, char_pos, font_size, color_adjust, surface);
        }
    }

    pub fn draw_symbol(&self, symbol: u8, pos: Vector3, font_size: f32, adjust_color: Vector4, surface: &mut Frame) {
        let (x, y) = to_2_dimensional(symbol as i32 - self.font_atlas.top_left_ascii_char, self.font_atlas.row_cnt);

        let symbol_width_on_texture = 1.0 / self.font_atlas.row_cnt as f32;
        let symbol_height_on_texture = 1.0 / self.font_atlas.row_cnt as f32;

        let left_on_texture = x as f32 * symbol_width_on_texture;
        let right_on_texture = left_on_texture + symbol_width_on_texture;
        let top_on_texture = y as f32 * -symbol_height_on_texture;
        let bottom_on_texture = top_on_texture - symbol_height_on_texture;

        let half_size = font_size / 2.0;

        let vertex1 = VertexWithTexcoord {
            position: [pos.x - half_size, pos.y - half_size, pos.z],
            texcoord: [left_on_texture, bottom_on_texture],
        };
        let vertex2 = VertexWithTexcoord {
            position: [pos.x - half_size, pos.y + half_size, pos.z],
            texcoord: [left_on_texture, top_on_texture],
        };
        let vertex3 = VertexWithTexcoord {
            position: [pos.x + half_size, pos.y + half_size, pos.z],
            texcoord: [right_on_texture, top_on_texture],
        };
        let vertex4 = VertexWithTexcoord {
            position: [pos.x + half_size, pos.y - half_size, pos.z],
            texcoord: [right_on_texture, bottom_on_texture],
        };
        self.textured_square_vertex.write(&[vertex1, vertex2, vertex3, vertex4]);

        let uniforms = uniform! { u_texture: &self.font_atlas.texture, u_adjust_color: adjust_color.as_array() };
        let prams = glium::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            ..Default::default()
        };
        surface.draw(&self.textured_square_vertex, &self.square_indices, &self.program_for_text, &uniforms, &prams).unwrap_or(());
    }
}
