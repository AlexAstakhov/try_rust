#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

mod ai_entity;
mod entity;
mod game;
mod game_object;
mod math;
mod maze_generator;
mod paint;
mod user_entity;
mod world;

use crate::entity::EntityAction;
use crate::math::fps::Fps;
use crate::math::vector4::Vector4;

use crate::game::*;
use crate::paint::painter::Painter;
extern crate scoped_pool;
use scoped_pool::Pool;
extern crate num_cpus;
use std::time::Instant;
#[macro_use]
extern crate glium;
use crate::math::vector3::Vector3;
use glium::{glutin, Surface};

fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new();
    let context = glutin::ContextBuilder::new().with_depth_buffer(24).with_multisampling(4);
    let display = Box::new(glium::Display::new(window, context, &events_loop).unwrap());

    let mut thread_pool = Pool::new(num_cpus::get());

    let mut game = Box::new(Game::new());

    let mut painter = Box::new(Painter::new(&display));

    let mut show_debug_info = true;

    let mut elapsed_microsecs_from_begin = 0u64;
    let mut dt = 0u32;

    let mut fps = Fps::new();

    let mut closed = false;

    while !closed {
        let frame_begin_time = Instant::now();

        //---------------------------------process events-------------------------------------------
        let mut user_action_changing = None;

        events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => match event {
                glutin::WindowEvent::CloseRequested => closed = true,
                ev => {
                    let input = match ev {
                        glutin::WindowEvent::KeyboardInput { input, .. } => input,
                        _ => return,
                    };

                    let key = match input.virtual_keycode {
                        Some(key) => key,
                        None => return,
                    };

                    if input.state == glutin::ElementState::Pressed {
                        match key {
                            glutin::VirtualKeyCode::Escape => closed = true,

                            glutin::VirtualKeyCode::Left => user_action_changing = Some(EntityAction::Left),
                            glutin::VirtualKeyCode::Right => user_action_changing = Some(EntityAction::Right),
                            glutin::VirtualKeyCode::D => show_debug_info = !show_debug_info,

                            _ => (),
                        };
                    } else {
                        user_action_changing = Some(EntityAction::Nothing);
                    }
                }
            },
            _ => (),
        });

        game.step(user_action_changing, elapsed_microsecs_from_begin, dt, &mut thread_pool, &mut painter.camera);

        {
            let mut target = display.draw();
            target.clear_color(0.003, 0.01, 0.008, 1.0);

            game.draw(&mut painter, &mut target, show_debug_info);

            if show_debug_info {
                painter.draw_text(&format!("FPS: {:.2}", fps.val()), Vector3::new(0.72, -0.95, 0.0), 0.05, Vector4::new(1.0, 1.0, 1.0, 1.0), &mut target);
            }

            match target.finish() {
                Ok(_) => (),
                Err(e) => println!("{}", e),
            }
        }

        dt = frame_begin_time.elapsed().subsec_micros();
        elapsed_microsecs_from_begin += u64::from(dt);
        fps.calculate((elapsed_microsecs_from_begin as f32 / 1000.0) as u32);
    }
}
